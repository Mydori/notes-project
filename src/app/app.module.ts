import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { ReactiveFormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { LoginComponent } from './Components/login/login.component';
import { MainComponent } from './Components/main/main.component';
import { LoginService } from './Services/login.service';
import { FooterComponent } from './Components/Share/footer/footer.component';


import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { NotesComponent } from './Components/notes/notes.component';
import { HeaderComponent } from './Components/Share/header/header.component';
import { AuthGuard } from './Guard/Auth.guard';


@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    MainComponent,
    FooterComponent,
    NotesComponent,
    HeaderComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FontAwesomeModule,
    ReactiveFormsModule,
    HttpClientModule
  ],
  providers: [
    LoginService,
    AuthGuard],
  bootstrap: [AppComponent]
})
export class AppModule { }
