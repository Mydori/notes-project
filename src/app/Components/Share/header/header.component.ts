import { Component, OnInit,Input } from '@angular/core';
import { faClose } from '@fortawesome/free-solid-svg-icons';
import { Router } from '@angular/router';
import { LoginService } from 'src/app/Services/login.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  faClose = faClose
  nombreUsuario:string = "";

  constructor(
    private router: Router,  private authService: LoginService) {
      this.nombreUsuario = (localStorage.getItem("token")!);
     }

  ngOnInit(): void {
  }

  CerrarSesion(){
    localStorage.removeItem('isLoggedIn');
    localStorage.removeItem('token');
    this.router.navigate([''], { skipLocationChange: true, });
  }

  logout() {
    this.authService.logout();
    this.router.navigate(['/login']);
  }


}
