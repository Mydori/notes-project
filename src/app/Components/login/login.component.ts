import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
// Interfaces
import { interfaceIniciarSesion } from 'src/app/Interfaces/login';
// Validaciones
import * as mensajesString from 'src/app/Components/Share/Strings/strings.componet';
import { TokenStorageService } from 'src/app/Services/token-storage.service';
import { LoginService } from 'src/app/Services/login.service';
import { AuthGuard } from 'src/app/Guard/Auth.guard';
import { MainComponent } from "src/app/Components/main/main.component";


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  // Variables
	formIniciarSesion: FormGroup;
  formRegistraUsuario :FormGroup;

  // interfaz para servicio
	_interfazIniciarSesion: interfaceIniciarSesion = {
		usuario: "",
		contrasenia: "",
	};

  // Cadenas para mensaje
   campoRequerido: String;
   longitudMinNombres: String;
   usuarioVar = "";
   mensajeRegistra = "";

   messageLogin:string = "";

   arrayUsariosRegistrados = [
    {
      "usuario":"admin",
      "contrasenia":"12345",
    },
    {
      "usuario":"admin2",
      "contrasenia":"12345",
    },
    {
      "usuario":"12345",
      "contrasenia":"12345",
    }
   ]


  constructor(
    private router: Router,
    private authService: LoginService, private tokenStorage: TokenStorageService,
    private AuthGuard:AuthGuard

    // public _ServiceLoginService: LoginService,
  ) {

    // IniciarSesion
    this.formRegistraUsuario = new FormGroup({
      usuario: new FormControl('', [ Validators.required, Validators.minLength(5),]),
      contrasenia: new FormControl('', [ Validators.required, Validators.minLength(5),]),
    });
    // IniciarSesion
    this.formIniciarSesion = new FormGroup({
      usuario: new FormControl('', [ Validators.required, Validators.minLength(5),]),
      contrasenia: new FormControl('', [ Validators.required, Validators.minLength(5),]),
    });

    this.campoRequerido = mensajesString.campoRequerido;
		this.longitudMinNombres = mensajesString.longitudMinNombres;

  }


  ngOnInit(): void {
    this.authService.logout();
  }

  IniciarSesion(): void {
    if (this.formIniciarSesion.invalid) {
      return;
    }
    else {
      for (let index = 0; index < this.arrayUsariosRegistrados.length; index++) {
        if (this.formIniciarSesion.controls["usuario"].value == this.arrayUsariosRegistrados[index].usuario &&
            this.formIniciarSesion.controls["contrasenia"].value == this.arrayUsariosRegistrados[index].contrasenia ) {
              localStorage.setItem('isLoggedIn', "true");
              console.log("Login successful");
              localStorage.setItem('token', this.formIniciarSesion.controls["usuario"].value);
              this.router.navigate(['PortalNotas/Main'], { skipLocationChange: true, });
        }
      }
    }

  }

  RegistrarUsuario(){
    this.usuarioVar = this.formRegistraUsuario.controls["usuario"].value;
    let contraseniaVar = this.formRegistraUsuario.controls["contrasenia"].value;
    let flagExiste = false;

    if (this.formRegistraUsuario.invalid) {
      return;
    }else{

      for (let index = 0; index < this.arrayUsariosRegistrados.length; index++) {
        if (this.formRegistraUsuario.controls["usuario"].value == this.arrayUsariosRegistrados[index].usuario ) {
           flagExiste = true;
           this.mensajeRegistra = "Este usuario esta registrado";
           this.LimpiarRegistro();
           return
        }
      }
      console.info(flagExiste)
      if (!flagExiste) {
        this.arrayUsariosRegistrados.push({
          usuario: this.usuarioVar,
          contrasenia: contraseniaVar,
        });
        localStorage.setItem('isLoggedIn', 'true');
        localStorage.setItem('token', this.formRegistraUsuario.controls["usuario"].value);
        localStorage.setItem('pass', this.formRegistraUsuario.controls["contrasenia"].value);

      this.router.navigate(['PortalNotas/Main'], { skipLocationChange: true, });
      this.LimpiarRegistro();
      }
    }
  }

  LimpiarRegistro(){
    this.formRegistraUsuario.controls['usuario'].setValue("");
    this.formRegistraUsuario.controls['contrasenia'].setValue("");
  }

}
