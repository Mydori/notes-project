import { Component, Input, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { faTrash, faPen, faPlus, faBoxArchive } from '@fortawesome/free-solid-svg-icons';
import { interfacCargareNotas } from 'src/app/Interfaces/notas';
// Interfaces
import { interfaceAgregarNota } from 'src/app/Interfaces/notas';

// Validaciones
import * as mensajesString from 'src/app/Components/Share/Strings/strings.componet';
import * as moment from 'moment';

@Component({
  selector: 'app-notes',
  templateUrl: './notes.component.html',
  styleUrls: ['./notes.component.css'],
})
export class NotesComponent implements OnInit {
  faTrash = faTrash;
  faPen = faPen;
  faPlus = faPlus;
  faBoxArchive = faBoxArchive;
  tempID = "";
  contador = 0;
  flagEditar = false;

  arrayNotasExistentes = [{
    "id": "1",
    "fechaAgregada": moment([2007, 0, 29]).format('LL'),
    "titulo": "Lorem Ipsum",
    "descripcion": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean id molestie sapien, eu auctor erat. "
  },
  {
    "id": "2",
    "fechaAgregada": moment([2008, 2, 8]).format('LL'),
    "titulo": "Mauris condimentum,",
    "descripcion": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean id molestie sapien, eu auctor erat. "
  },
  {
    "id": "3",
    "fechaAgregada": moment([2010, 6, 5]).format('LL'),
    "titulo": "Suspendisse ligula mi",
    "descripcion": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean id molestie sapien, eu auctor erat. "
  },
  {
    "id": "4",
    "fechaAgregada": moment([2003, 0, 29]).format('LL'),
    "titulo": "Prueba Notas",
    "descripcion": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean id molestie sapien, eu auctor erat. "
  }];
  arrayNotasArchivadas = [{
    "id": "1",
    "fechaAgregada": moment([2020, 11, 14]).format('LL'),
    "titulo": "Nota Archivada 1",
    "descripcion": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean id molestie sapien, eu auctor erat. "
  },
  {
    "id": "2",
    "fechaAgregada": moment([2022, 9, 15]).format('LL'),
    "titulo": "Nota  Archivada 1,",
    "descripcion": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean id molestie sapien, eu auctor erat. "
  },
  {
    "id": "3",
    "fechaAgregada": moment([2020, 4, 6]).format('LL'),
    "titulo": "Nota Archivada 3",
    "descripcion": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean id molestie sapien, eu auctor erat. "
  },];

  _interfazCargarNota: interfacCargareNotas = {
    id: '',
    fechaAgregada: '',
    titulo: '',
    descripcion: '',
  };

  formAgregarNuevaNota: FormGroup;

  // Cadenas para mensaje
   campoRequerido: String;
   longitudMinTitulo: String;
   mensajeCorreoFormato: String;
   longitudMinDescripcion:string;

   _interfazAgregarNota: interfaceAgregarNota = {
    titulo:'',
    descripcion: ''
   }

  constructor() {
    this.campoRequerido = mensajesString.campoRequerido;
		this.longitudMinTitulo = mensajesString.longitudMinTitulo;
		this.mensajeCorreoFormato = mensajesString.mensajeCorreoFormato;
    this.longitudMinDescripcion = mensajesString.longitudMinDescripcion;

    this.formAgregarNuevaNota = new FormGroup({
			titulo: new FormControl('', [Validators.required, Validators.minLength(5)]),
			descripcion: new FormControl('', [Validators.required, Validators.minLength(25), Validators.max(150)])
		});
  }

  ngOnInit(): void {
  }

  onKeyTextArea(event : any){
    this.contador = event.target.value.length;
  }

  onBlurInputUsuario(event : any){

    let cadena = this.formAgregarNuevaNota.controls['titulo'].value;
    let expresion =  /(?:[a-zA-Z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-zA-Z0-9!#$%&'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-zA-Z0-9](?:[a-zA-Z0-9-]*[a-zA-Z0-9])?\.)+[a-zA-Z0-9](?:[a-zA-Z0-9-]*[a-zA-Z0-9])?|\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-zA-Z0-9-]*[a-zA-Z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])/i;
    let esMail = cadena.match(expresion);
    if (esMail.length >= 1 ? true : false) {
      this.formAgregarNuevaNota.controls['titulo'].setValue("");
    }
  }

  LimpiarValores(){
    this.formAgregarNuevaNota.controls['titulo'].setValue("");
    this.formAgregarNuevaNota.controls['descripcion'].setValue("");
		this.formAgregarNuevaNota.markAsUntouched();

    // Reiniciar contador de caracteres Textarea
    this.contador = 0;
  }

  AgregarNotaNueva(caseButton:any, tempID:any){
      this._interfazAgregarNota.titulo = this.formAgregarNuevaNota.controls['titulo'].value;
      this._interfazAgregarNota.descripcion = this.formAgregarNuevaNota.controls['descripcion'].value;

       //id Asigando HTML al precionar un boton u otro
      // id  1 : boton agregar
      // id  2 : boton editar
      switch (caseButton) {
        case 1:
          // agregamos un ID  a la nueva nota
          let nuevoID = this.arrayNotasExistentes.length + 1;

          this.arrayNotasExistentes.push({
            id: (nuevoID + 1).toString(),
            titulo: this._interfazAgregarNota.titulo,
            descripcion: this._interfazAgregarNota.descripcion,
            fechaAgregada: moment().format('LL'),
          });
          break;

        case 2:
          let obtenerID =tempID;
          for (let i = 0; i < this.arrayNotasExistentes.length; i++) {
            if (tempID == this.arrayNotasExistentes[i].id) {
              this.arrayNotasExistentes[i].titulo = this._interfazAgregarNota.titulo;
              this.arrayNotasExistentes[i].descripcion = this._interfazAgregarNota.descripcion;
            }
          }
          break;
      }

      console.info("this.arrayNotasExistentes.length")
      console.info(this.arrayNotasExistentes.length)
      console.info(this.arrayNotasExistentes);

      //limpiar formulario
      this.LimpiarValores();

  }

  EliminarNota(id:any){
    for (let i = 0; i < this.arrayNotasExistentes.length; i++) {
      if (id == this.arrayNotasExistentes[i].id) {
        this.arrayNotasExistentes.splice(i,1);
      }
    }
  }

  EditarNota(id:any){
    this.flagEditar = true;
    this.tempID = id; // para asigar al boton de agregar
    console.info("editar id " + id)
    for (let i = 0; i < this.arrayNotasExistentes.length; i++) {
      if (id == this.arrayNotasExistentes[i].id) {
        this.formAgregarNuevaNota.controls['titulo'].setValue(this.arrayNotasExistentes[i].titulo);
        this.formAgregarNuevaNota.controls['descripcion'].setValue(this.arrayNotasExistentes[i].descripcion);
      }
    }
  }

  ArchivarNota(id:any){
    console.info("id")
    console.info(id)
    for (let i = 0; i < this.arrayNotasExistentes.length; i++) {
      if (id == this.arrayNotasExistentes[i].id) {
        console.info(this.arrayNotasExistentes[i]);

        // agregamos un nuevo ID  a la  nota archivada
        let nuevoID = this.arrayNotasArchivadas.length + 1;

        this.arrayNotasArchivadas.push({
          id: (nuevoID + 1).toString(),
          titulo: this.arrayNotasExistentes[i].titulo,
          descripcion: this.arrayNotasExistentes[i].descripcion,
          fechaAgregada: moment().format('LL'),
        });

        this.arrayNotasExistentes.splice(i,1);
      }
    }
  }

  RestaurarNota(id:any){
    console.info("id")
    console.info(id)
    for (let i = 0; i < this.arrayNotasArchivadas.length; i++) {
      if (id == this.arrayNotasArchivadas[i].id) {
        console.info(this.arrayNotasArchivadas[i]);

        // agregamos un nuevo ID  a la  nota archivada
        let nuevoID = this.arrayNotasExistentes.length + 1;

        this.arrayNotasExistentes.push({
          id: (nuevoID + 1).toString(),
          titulo: this.arrayNotasArchivadas[i].titulo,
          descripcion: this.arrayNotasArchivadas[i].descripcion,
          fechaAgregada: moment().format('LL'),
        });

        this.arrayNotasArchivadas.splice(i,1);
      }
    }
  }
}
