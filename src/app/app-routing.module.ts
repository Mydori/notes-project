import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './Components/login/login.component';
import { MainComponent } from './Components/main/main.component';
import { AuthGuard } from './Guard/Auth.guard';

const routes: Routes = [
	{ path: '', component: LoginComponent },
  {
    path: 'PortalNotas/Main', component: MainComponent, canActivate : [AuthGuard]
  },

	{ path: '**', pathMatch: 'full', redirectTo: '' }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
