
export interface interfaceAgregarNota {
	titulo: string;
	descripcion: string;
}

export interface interfacCargareNotas {
  id: string,
  fechaAgregada: string,
  titulo: string,
  descripcion: string
}
